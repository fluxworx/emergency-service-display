package org.fluxworx.notdienstanzeige.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "notdienst")
public class Pharmacy {

    @Element(name = "datum", required = false)
    private String date;

    @Element(name = "apotheke")
    private String name;

    @Element(name = "strasse")
    private String street;

    @Element(name = "plz")
    private String postcode;

    @Element(name = "ort")
    private String city;

    @Element(name = "telefon", required = false)
    private String phone;

    @Element(name = "latitude", required = false)
    private String latitude;

    @Element(name = "longitude", required = false)
    private String longitude;

    @Element(name = "ortsteil", required = false)
    private String destrict;

    public String getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    public String getPostcode() {
        return postcode;
    }

    public String getCity() {
        return city;
    }

    public String getPhone() {
        return phone;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getDestrict() {
        return destrict;
    }
}
