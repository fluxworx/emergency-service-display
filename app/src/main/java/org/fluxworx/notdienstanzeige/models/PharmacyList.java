package org.fluxworx.notdienstanzeige.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "notdienste")
public class PharmacyList {

    @Element(name = "beschreibung", required = false)
    private String description;

    @Element(name = "zeitraum", required = false)
    private String period;

    @Element(name = "erstellungsdatum", required = false)
    private String creationDate;

    @ElementList(inline = true)
    private List<Pharmacy> pharmacies;

    public List<Pharmacy> getPharmacies() {
        return pharmacies;
    }
}
