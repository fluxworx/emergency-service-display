package org.fluxworx.notdienstanzeige.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;

import org.fluxworx.notdienstanzeige.R;
import org.fluxworx.notdienstanzeige.adapters.PharmacyAdapter;
import org.fluxworx.notdienstanzeige.models.Pharmacy;
import org.fluxworx.notdienstanzeige.models.PharmacyList;
import org.springframework.http.converter.xml.SimpleXmlHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ShowPharmaciesActivity extends AppCompatActivity {

    private final String BASE_URL = "https://www.akwl.de/notdienst/xml.php";
    private final String NUMBER_OF_PHARMACIES = "5";

    private double longitudeGPS;
    private double latitudeGPS;

    private LocationManager locationManager;
    private final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-M-d");

    private Handler mHandler = new Handler();
    TextView mTextViewLastUpdated;
    ListView listView;

    PharmacyAdapter pharmacyAdapter;
    protected List<Pharmacy> pharmacies = new ArrayList<Pharmacy>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_pharmacies);

        mTextViewLastUpdated = (TextView) findViewById(R.id.TextViewLastUpdated);
        pharmacyAdapter = new PharmacyAdapter(this, pharmacies);
        listView = (ListView) findViewById(R.id.listview);
        listView.setAdapter(pharmacyAdapter);

        initLocation();
    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void initLocation() {

        // Get the location manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean isLocationEnabled = isLocationEnabled();
        if (!isLocationEnabled) {
            showAlert();
        }

        if (Build.VERSION.SDK_INT < 23 || ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, 2 * 60 * 1000, 10, locationListenerGPS);
        }
    }

    private final LocationListener locationListenerGPS = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitudeGPS = location.getLongitude();
            latitudeGPS = location.getLatitude();

            mHandler.removeCallbacks(mUpdateTimeTask);
            mHandler.postDelayed(mUpdateTimeTask, 100);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            AsyncTaskRunner task = new AsyncTaskRunner();
            task.execute();

//    	        mHandler.postDelayed(this, 1000 * 60); // update every 1 min

            // update every day at 9:00 AM
            long oneDay = (long) 1000.0 * 60 * 60 * 24;
            Date nextUpdate = new Date(System.currentTimeMillis() + oneDay);
            nextUpdate.setHours(9);
            nextUpdate.setMinutes(0);
            nextUpdate.setSeconds(0);

            long uptimeMillis = SystemClock.uptimeMillis();
            long systemMillis = new Date().getTime();
            long deltaMillis = systemMillis - uptimeMillis;

            mHandler.postAtTime(this, nextUpdate.getTime() - deltaMillis);
        }
    };


    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {

            String currentDate = DATE_FORMAT.format(new Date());

            // The connection URL
            String url = String.format("%s?a=%s&m=koord&w=%s;%s&z=%s;%s",
                    BASE_URL, NUMBER_OF_PHARMACIES, latitudeGPS, longitudeGPS, currentDate, currentDate);

            // Create a new RestTemplate instance
            RestTemplate restTemplate = new RestTemplate();

            // Add the String message converter
            restTemplate.getMessageConverters().add(new SimpleXmlHttpMessageConverter());

            // Make the HTTP GET request, marshaling the response to a String
            final PharmacyList pharmacyList = restTemplate.getForObject(url, PharmacyList.class);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pharmacyAdapter.clear();
                    pharmacyAdapter.addAll(pharmacyList.getPharmacies());
                }
            });

            return null;
        }


        @Override
        protected void onPostExecute(String result) {

            SimpleDateFormat sdf = new SimpleDateFormat();
            sdf.applyPattern("EEEE', 'dd. MMMM yyyy 'um' HH:mm:ss");
            mTextViewLastUpdated.setText("Letzte Aktualisierung: " + sdf.format(new Date()));
        }

    }
}
