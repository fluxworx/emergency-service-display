package org.fluxworx.notdienstanzeige.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.fluxworx.notdienstanzeige.R;
import org.fluxworx.notdienstanzeige.models.Pharmacy;

import java.util.List;

public class PharmacyAdapter extends ArrayAdapter<Pharmacy> {
    public PharmacyAdapter(@NonNull Context context, @NonNull List<Pharmacy> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Get the data item for this position
        Pharmacy pharmacy = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_pharmacy, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
        TextView tvStreet = (TextView) convertView.findViewById(R.id.tvStreet);
        TextView tvCity = (TextView) convertView.findViewById(R.id.tvCity);
        TextView tvPhone = (TextView) convertView.findViewById(R.id.tvPhone);

        // Populate the data into the template view using the data object
        tvName.setText(pharmacy.getName());
        tvStreet.setText(pharmacy.getStreet());
        String cityAndDestrict = pharmacy.getCity();
        if (null != pharmacy.getDestrict() && !pharmacy.getDestrict().isEmpty()) {
            cityAndDestrict = String.format("%s-%s", pharmacy.getCity(), pharmacy.getDestrict());
        }
        tvCity.setText(cityAndDestrict);
        tvPhone.setText(pharmacy.getPhone());

        // Return the completed view to render on screen
        return convertView;
    }
}
